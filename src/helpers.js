export function createElementFromHTML (htmlString) {
  var div = document.createElement('div');
  div.innerHTML = htmlString.trim();

  return div;
}

export function ready(fn) {
  setTimeout(function() {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  }, 0);
}

export function showComponent(hash) {
  let elements = document.querySelectorAll('#about, #settings, #about-edit');

  elements.forEach(function(el) {
    el.classList.add('hidden');
  })

  elements.forEach(function(el) {
    if (hash === '#about' && el.id === 'about'
    || hash === '#settings' && el.id === 'settings'
    || hash === '#about-edit' && el.class === 'about-edit') {
      el.classList.remove('hidden');
    }
  });
}

export function insertValueHTML(elementArray, value) {
  elementArray.forEach(function(el) {
    el.innerHTML = value;
  });
}