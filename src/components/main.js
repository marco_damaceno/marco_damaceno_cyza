import '@/styles/main.scss';
import about from '@/components/about';
import aboutEdit from '@/components/aboutEdit';
import settings from '@/components/settings';

let template = `
  <main>
    <div id="content">
      <div id="about" class="hidden"></div>
      <div id="about-edit" class="hidden"></div>
      <div id="settings" class="hidden"></div>
    </div>
  </main>
`

document.addEventListener("DOMContentLoaded", function() {
  this.querySelector('#about').innerHTML = about;
  this.querySelector('#about-edit').innerHTML = aboutEdit;
  this.querySelector('#settings').innerHTML = settings;
});

export default template;