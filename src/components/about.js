import '@/styles/about.scss';
import '@/styles/popup.scss';
import { ready, showComponent } from '@/helpers';

let template = `
  <div class="about">
    <div class="actions">
      <a href="#about-edit" class="edit-info"><i class="icon ion-edit"></i></a>
    </div>
    <h2>About</h2>
    <h3><span class="pad bg-grey"><span class="firstname-txt">Jessica</span> <span class="lastname-txt">Parker</span></span><span class="pencil-edit pad" id="edit-name"><i class="icon ion-edit"></i></span></h3>
    <div class="location">
      <ul>
        <li>
          <span class="pad bg-grey">
            <i class="icon ion-android-globe"></i>
            <span class="website-txt">www.seller.com</span>
          </span>
          <span class="pencil-edit pad" id="edit-website">
            <i class="icon ion-edit"></i>
          </span>
        </li>
        <li>
          <span class="pad bg-grey">
            <i class="icon ion-ios-telephone-outline"></i>
            <span class="phone-number-txt">(949) 325 - 68594</span>
          </span>
          <span class="pencil-edit pad" id="edit-phone-number">
            <i class="icon ion-edit"></i>
          </span>
        </li>
        <li>
          <span class="pad bg-grey">
            <i class="icon ion-ios-home"></i>
            <span class="location-txt">Newport Beach CA</span>
          </span>
          <span class="pencil-edit pad" id="edit-location">
            <i class="icon ion-edit"></i>
          </span>
        </li>
      </ul>
      <div class="popup">
        <span class="popuptext" id="myPopup">Popup text...</span>
      </div>
    </div>
  </div>
`

function editInfoUser() {
  document.querySelector('#about').classList.add('hidden');
  document.querySelector('#settings').classList.add('hidden');
  document.querySelector('#about-edit').classList.remove('hidden');
}

function openPopup (ev) {
  ev.preventDefault();
  let popup = document.querySelector("#myPopup");
  popup.classList.toggle("show");
}

ready(function() {
  let element = document.querySelector('.edit-info');
  let editNameElem = document.querySelector('#edit-name');
  let editWebsiteElem = document.querySelector('#edit-website');
  let editPhoneNumberElem = document.querySelector('#edit-phone-number');
  let editLocationElem = document.querySelector('#edit-location');

  editNameElem.addEventListener('click', openPopup);
  editWebsiteElem.addEventListener('click', openPopup);
  editPhoneNumberElem.addEventListener('click', openPopup);
  editLocationElem.addEventListener('click', openPopup);

  element.addEventListener('click', function(ev) {
    ev.preventDefault();
    window.location.hash = element.hash;
    showComponent(element.hash);
    editInfoUser();
  });
});

export default template;
