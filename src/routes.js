export function router(el) {
  let hash = window.location.hash;

  if (hash === '#about' && el.id === 'about'
  || hash === '#settings' && el.id === 'settings') {
    el.classList.remove('hidden');
  }
}