import '@/styles/menu.scss';
import { ready, showComponent } from '@/helpers';

let template = `
    <nav class="main-menu">
      <div class="menu">
        <a href="#about">About</a>
        <a href="#settings">Settings</a>
        <a href="#options">Option1</a>
        <a href="#options">Option2</a>
        <a href="#options">Option3</a>
      </div>
    </nav>
`;

function getMenuLinks() {
  return document.querySelectorAll('.menu a');
}

function removeActiveClass(menu) {
  menu.classList.remove('active');
}

function addActiveClass(menu) {
  menu.classList.add('active');
}

ready(function() {
  let menuLinks = getMenuLinks();

  menuLinks.forEach(function(menu) {
    if (menu.hash === window.location.hash) {
      addActiveClass(menu);
      showComponent(menu.hash);
    }
  });

  menuLinks.forEach(function(menu) {
    menu.addEventListener('click', function(ev) {
      ev.preventDefault();

      window.location.hash = this.hash;

      showComponent(this.hash);

      menuLinks.forEach(removeActiveClass);
      if (window.location.hash === this.hash) {
        addActiveClass(menu);
      }
    })
  });
})

export default template;
