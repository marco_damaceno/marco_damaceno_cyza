import { createElementFromHTML, ready, insertValueHTML } from '@/helpers';
import '@/styles/reset.css';
import '@/styles/default.scss';
import header from '@/components/header';
import menu from '@/components/menu';
import main from '@/components/main';
import footer from '@/components/footer';

let template =
  '<div id="main-header">'
  .concat(header)
  .concat(menu)
  .concat('</div>')
  .concat(main)

let appElem = document.querySelector('#app');

ready(function() {
  localStorage.setItem('profile', JSON.stringify({
    firstname: 'Jessica',
    lastname: 'Parker',
    website: 'www.seller.com',
    phoneNumber: '(949) 325 - 68594',
    location: 'Newport Beach CA'
  }));
  let profile = JSON.parse(localStorage.getItem('profile'));
  insertValueHTML(document.querySelectorAll('.firstname-txt'), profile.firstname);
  insertValueHTML(document.querySelectorAll('.lastname-txt'), profile.lastname);
  insertValueHTML(document.querySelectorAll('.website-txt'), profile.website);
  insertValueHTML(document.querySelectorAll('.phone-number-txt'), profile.phoneNumber);
  insertValueHTML(document.querySelectorAll('.location-txt'), profile.location);
});

appElem.appendChild(createElementFromHTML(template))
