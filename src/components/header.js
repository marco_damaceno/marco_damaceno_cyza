import logout from '@/components/logout';
import infoUser from '@/components/infoUser';
import rating from '@/components/rating';
import reviews from '@/components/reviews';
import followers from '@/components/followers';
import profilePhoto from '@/components/profilePhoto';

let template = '<header>'
  .concat(logout)
  .concat(profilePhoto)
  .concat('<div class="info">')
  .concat(infoUser)
  .concat('<div class="rating-reviews-group">')
  .concat(rating)
  .concat(reviews)
  .concat('</div>')
  .concat(followers)
  .concat('</div>')
  .concat('</header>')

export default template