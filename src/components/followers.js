import { ready } from '@/helpers'
import '@/styles/followers.scss';

let template = `
  <div class="followers">
    <ul>
      <li><span id="follow" class="jsd uahsd"><i class="icon ion-plus-circled"></i></span> <span class="followers-number">15</span> Followers</li>
    </ul>
  </div>
`

ready(function() {
  let followLink = document.querySelector('#follow');
  let followersNumber = document.querySelector('.followers-number');
  let icon = followLink.querySelector('.icon');
  followLink.addEventListener('click', function(ev) {
    ev.preventDefault();
    if (followLink.classList.value.indexOf('unfollow') < 0) {
      followersNumber.innerText = parseInt(followersNumber.innerText) + 1;
      followLink.classList.add('unfollow');
      icon.classList.remove('ion-plus-circled')
      icon.classList.add('ion-minus-circled')
    } else {
      followersNumber.innerText = parseInt(followersNumber.innerText) - 1;
      followLink.classList.remove('unfollow');
      icon.classList.remove('ion-minus-circled')
      icon.classList.add('ion-plus-circled')
    }
  })
});

export default template;