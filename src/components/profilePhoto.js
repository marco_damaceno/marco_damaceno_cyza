import '@/styles/profilePhoto.scss';
import profileImage from '@/images/profile_image.jpg';

let template = `
  <div class="profile-identity">
    <div class="profile-image">
      <img src="${profileImage}" />
    </div>
    <div class="profile-name">
      <h1><span class="firstname-txt">Jessica</span> <span class="lastname-txt">Parker</span></h1>
    </div>
  </div>
`

export default template