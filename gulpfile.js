(function () {
  'use strict';

  const gulp = require('gulp')
  const webpack = require('webpack');
  const webpackOptions = require('./webpack.config');
  const WebpackDevServer = require('webpack-dev-server');

  let port = 8080;
  let host = '0.0.0.0';

  let paths = {
    js: [ './src/**/*.js' ],
    html: [ './src/**/*.html' ],
    css: [
      './src/**/*.css',
      './src/**/*.scss',
    ],
  };

  gulp.task('build', function (callback) {
    webpack(webpackOptions, function (error, stats) {
      callback();
    });
  });

  gulp.task('serve', function (callback) {
    let compiler = webpack(webpackOptions);
    new WebpackDevServer(compiler, webpackOptions.devServer)
      .listen(port, host, function () {});
  });

  gulp.task('watch', function (callback) {
    gulp.watch(
        paths.js
        .concat(paths.html)
        .concat(paths.css), ['build']);
  });

  gulp.task('default', ['serve']);
})();

