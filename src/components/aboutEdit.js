import { ready, insertValueHTML } from '@/helpers';

let template = `
  <div class="about">
    <div class="actions">
      <a href="#about" id="about-cancel">CANCEL</a>
      <a href="#" id="about-save">SAVE</a>
    </div>
    <h2>About</h2>
    <form>
      <input name="firstname" placeholder="FIRST NAME *" type="text" required="">
      <input name="lastname" placeholder="LAST NAME *" type="text" required="">
      <input name="website" placeholder="WEBSITE *" type="text" required="">
      <input name="phone-number" placeholder="PHONE NUMBER *" type="number" required="">
      <input name="location" placeholder="CITY, STATE & ZIP *" type="text" required="">
    </form>
  </div>
`

function saveData(params) {
  return new Promise(function(resolve, reject) {
    localStorage.setItem('profile', JSON.stringify(params));
    resolve(params);
  });
}

ready(function() {
  let profile = JSON.parse(localStorage.getItem('profile'));

  function setProfileHTML(profile) {
    insertValueHTML(document.querySelectorAll('.firstname-txt'), profile.firstname)
    insertValueHTML(document.querySelectorAll('.lastname-txt'), profile.lastname)
    insertValueHTML(document.querySelectorAll('.website-txt'), profile.website)
    insertValueHTML(document.querySelectorAll('.phone-number-txt'), profile.phoneNumber)
    insertValueHTML(document.querySelectorAll('.location-txt'), profile.location)
  }

  let firstnameInput = document.querySelector('input[name=firstname]');
  let lastnameInput = document.querySelector('input[name=lastname]');
  let websiteInput = document.querySelector('input[name=website]');
  let phoneNumberInput = document.querySelector('input[name=phone-number]');
  let locationInput = document.querySelector('input[name=location]');

  firstnameInput.value = profile.firstname;
  lastnameInput.value = profile.lastname;
  websiteInput.value = profile.website;
  phoneNumberInput.value = profile.phoneNumber;
  locationInput.value = profile.location;

  let saveBtn = document.querySelector('#about-save');
  saveBtn.addEventListener('click', function(ev) {
    ev.preventDefault();

    if (firstnameInput.value !== ''
      || lastnameInput.value !== ''
      || websiteInput.value !== ''
      || phoneNumberInput.value !== ''
      || locationInput.value !== '') {
      let paramsToSave = {
        firstname: firstnameInput.value,
        lastname: lastnameInput.value,
        website: websiteInput.value,
        phoneNumber: phoneNumberInput.value,
        location: locationInput.value
      }

      // Saviing
      saveData(paramsToSave)
        .then(setProfileHTML)
        .catch(err => console.log(err));

    } else {
      alert('Required fields!');
    }
  })
});

export default template;