import '@/styles/infoUser.scss';

let template = `
  <div class="info-user">
    <ul>
      <li><i class="icon ion-ios-telephone-outline"></i><span class="phone-number-txt">(949) 325 - 68594</span></li>
      <li><i class="icon ion-ios-location-outline"></i><span class="location-txt">Newport Beach CA</span></li>
    </ul>
  </div>
`

export default template;