import '@/styles/rating.scss';

let template = `
  <div class="rating">
    <p>
      <i class="icon ion-android-star"></i>
      <i class="icon ion-android-star"></i>
      <i class="icon ion-android-star"></i>
      <i class="icon ion-android-star"></i>
      <i class="icon ion-ios-star-outline"></i>
    </p>
  </div>
`

export default template;